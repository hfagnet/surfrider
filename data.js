tabProduits =
[
    {
        "nouveau": true,
        "type": "Pull",
        "numProd": "1",
        "libProd":"The Logo Hooded Zipper Unisex",
        "prixProd": 69,
        "tailleProd": ["XS","S","M","L","XL"],
        "couleurProd": ["Noir"],
        "images": ["images/logo_hooded_zipper_unisex.jpg","images/logo_hooded_zipper_unisex2.jpg","images/logo_hooded_zipper_unisex3.jpg","images/logo_hooded_zipper_unisex4.jpg","images/logo_hooded_zipper_unisex5.jpg"]
    },
    {
        "nouveau": true,
        "type": "Pull",
        "numProd": "2",
        "libProd":"The Committed Hooded Sweatshirt Unisex",
        "prixProd": 65,
        "tailleProd": ["XS","S","M","L","XL"],
        "couleurProd": ["Vert"],
        "images": ["images/committed_hooded_sweatshirt_unisex.jpg","images/committed_hooded_sweatshirt_unisex2.jpg","images/committed_hooded_sweatshirt_unisex3.jpg","images/committed_hooded_sweatshirt_unisex4.jpg","images/committed_hooded_sweatshirt_unisex5.jpg"]
    },
    {
        "nouveau": false,
        "type": "Pull",
        "numProd": "3",
        "libProd":"The Logo Hooded Sweatshirt Women",
        "prixProd": 65,
        "tailleProd": ["XS","S","M","L","XL"],
        "couleurProd": ["Noir"],
        "images": ["images/logoh_hooded_sweatshirt_women.jpg","images/logoh_hooded_sweatshirt_women2.jpg","images/logoh_hooded_sweatshirt_women3.jpg","images/logoh_hooded_sweatshirt_women4.jpg"]
    },
    {
        "nouveau": true,
        "type": "Pull",
        "numProd": "3",
        "libProd":"The Logo Sweater Women",
        "prixProd": 59,
        "tailleProd": ["XS","S","M","L","XL"],
        "couleurProd": ["Bleu"],
        "images": ["images/logo_sweater_women.jpg","images/logo_sweater_women2.jpg","images/logo_sweater_women3.jpg","images/logo_sweater_women4.jpg","images/logo_sweater_women5.jpg"]
    },
    {
        "nouveau": false,
        "type": "Pull",
        "numProd": "4",
        "libProd":"The Summer Hooded Sweatshirt Unisex",
        "prixProd": 65,
        "tailleProd": ["XS","S","M","L","XL"],
        "couleurProd": ["Beige"],
        "images": ["images/summer_hooded_sweatshirt_unisex.jpg","images/summer_hooded_sweatshirt_unisex2.jpg","images/summer_hooded_sweatshirt_unisex3.jpg"]
    },

    {
        "nouveau": true,
        "type": "Tee",
        "numProd": "5",
        "libProd":"The Logo Tee Unisex",
        "prixProd": 29,
        "tailleProd": ["XS","S","M","L","XL"],
        "couleurProd": ["Blanc"],
        "images": ["images/logo_tee_unisex.jpg","images/logo_tee_unisex2.jpg","images/logo_tee_unisex3.jpg","images/logo_tee_unisex4.jpg","images/logo_tee_unisex5.jpg"]
    },
    {
        "nouveau": false,
        "type": "Tee",
        "numProd": "6",
        "libProd":"The Summer Tee Unisex",
        "prixProd": 29,
        "tailleProd": ["XS","S","M","L","XL"],
        "couleurProd": ["Orange"],
        "images": ["images/logo_summer_tee_unisex.jpg","images/logo_summer_tee_unisex2.jpg","images/logo_summer_tee_unisex3.jpg","images/logo_summer_tee_unisex4.jpg","images/logo_summer_tee_unisex5.jpg"]
    },
    {
        "nouveau": false,
        "type": "Tee",
        "numProd": "7",
        "libProd":"The 90's Tee Unisex",
        "prixProd": 35,
        "tailleProd": ["XS","S","M","L","XL"],
        "couleurProd": ["Bleu"],
        "images": ["images/90s_tee_unisex.jpg","images/90s_tee_unisex2.jpg","images/90s_tee_unisex3.jpg","images/90s_tee_unisex4.jpg","images/90s_tee_unisex5.jpg"]
    },
    {
        "nouveau": true,
        "type": "Tee",
        "numProd": "8",
        "libProd":"The Engaged Tee Unisex",
        "prixProd": 29,
        "tailleProd": ["XS","S","M","L","XL"],
        "couleurProd": ["Vert"],
        "images": ["images/engaged_tee_unisex.jpg","images/engaged_tee_unisex2.jpg","images/engaged_tee_unisex3.jpg","images/engaged_tee_unisex4.jpg","images/engaged_tee_unisex5.jpg"]
    },
    {
        "nouveau": true,
        "type": "Tee",
        "numProd": "9",
        "libProd":"The Manifesto Tee Unisex",
        "prixProd": 29,
        "tailleProd": ["XS","S","M","L","XL"],
        "couleurProd": ["Beige"],
        "images": ["images/manifesto_tee_unisex.jpg","images/manifesto_tee_unisex2.jpg","images/manifesto_tee_unisex3.jpg","images/manifesto_tee_unisex4.jpg","images/manifesto_tee_unisex5.jpg"]
    },
    {
        "nouveau": true,
        "type": "Tee",
        "numProd": "10",
        "libProd":"The Summer Tee Women",
        "prixProd": 29,
        "tailleProd": ["XS","S","M","L","XL"],
        "couleurProd": ["Rose"],
        "images": ["images/summer_tee_women.jpg","images/summer_tee_women2.jpg","images/summer_tee_women3.jpg"]
    },
    {
        "nouveau": false,
        "type": "Tee",
        "numProd": "11",
        "libProd":"The Marinière For Women",
        "prixProd": 35,
        "tailleProd": ["XS","S","M","L","XL"],
        "couleurProd": ["White"],
        "images": ["images/mariniere_for_women.jpg","images/mariniere_for_women2.jpg","images/mariniere_for_women3.jpg","images/mariniere_for_women4.jpg"]
    }
]