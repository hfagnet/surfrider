/*----------------------------------------------
------------------------------------------------
----                Initialisation          ----
------------------------------------------------
-----------------------------------------------*/
    // Remplissage du catalogue avec tous les produits
var catalogue = document.querySelector('#catalogue');
var toutLesProduits = []
function displayAll()
{
    toutLesProduits = createProds(tabProduits);
    fillCatalogue(toutLesProduits);
    tableauCourant = toutLesProduits
}
displayAll();

    //Déclaration du tableau "courant", représente la liste des produits ACTUELLEMENT affichés à l'écran
    //  - Permet d'additionner des filtres
    //  - Est donc actualisé à chaque changement d'affichage du tableau
var tableauCourant = toutLesProduits;
// Fin de l'initialisation



//Rempli le catalogue avec le tableau final
// paramètre : un tableau (déjà trié s'il le faut)
function fillCatalogue(tab)
{
    catalogue.innerHTML = '';
    for(i=0; i<=tab.length-1; i++)
    {
        let produit = document.createElement('div');
        produit.classList.add("produitcard", "d-flex", "flex-column", "my-5", "px-3","mx-3", "align-items-center", "col-4");
        produit.innerHTML = `

            <img class="mt-3" src="${tab[i].images[0]}" title="" alt="" width="300" />
            <p class="titreProduit col-12 mt-2">${tab[i].libProd}</p>
            <p class="descProduit col-12">${tab[i].libProd}</p>

            <div class="row col-12 d-flex">
                <div class="col-9 p-0 align-self-center">
                    <img src="images/etoilepleine.png" title="" alt="" width="35" />
                    <img src="images/etoilepleine.png" title="" alt="" width="35" />
                    <img src="images/etoilepleine.png" title="" alt="" width="35" />
                    <img src="images/etoilepleine.png" title="" alt="" width="35" />
                    <img src="images/etoilepleine.png" title="" alt="" width="35" />
                    <p>143 avis</p>
                </div>
                <p class="col-3 align-self-end descProduit">${tab[i].prixProd}€</p>
            </div>`;
        //Ajout du produit au catalogue
        catalogue.appendChild(produit);
    }
}




//Retourne tableau des produits (objet Produit)
// paramètre : tableau de data.js
// retourne : tableau des produits sous formes de class Produit
function createProds(tabProduits)
{
    var res = [];

    for(i=0; i<=tabProduits.length-1; i++)
    {
        let monProduit = new Produit(tabProduits[i].type, tabProduits[i].numProd, tabProduits[i].libProd, tabProduits[i].prixProd, tabProduits[i].tailleProd, tabProduits[i].couleurProd, tabProduits[i].images)
        res.push(monProduit);
    }
    return res;
}

/*----------------------------------------------------------
------------------------------------------------------------
----                Fonctions de Filtrage               ----
------------------------------------------------------------
----------------------------------------------------------*/


//Retourne les produits d'un type donné (pull ou tee)
//paramètres :
//  type: le type à sélectionner au filtre
//  tab: tableau d'objet Produit
function getProdsbyType(type, tab)
{
    var res = tab.slice().filter(produit=>produit.type == type);

    return res;
}

//Retourne les produits d'un type donné (pull ou tee)
//paramètres :
//  couleur: la couleur à sélectionner au filtre
//  tab: tableau d'objet Produit
function getProdsbyColor(couleur, tab)
{
    var res = tab.slice().filter(produit=>produit.couleurProd.includes(couleur));

    return res;
}

//Retourne les produits d'un type donné (pull ou tee)
//paramètres :
//  prix: Prix maximum à sélectionner au filtre
//  tab: tableau d'objet Produit
function getProdsbyPrix(prix, tab)
{
    var res = tab.slice().filter(produit=>produit.prixProd <= prix);

    return res;
}

/*------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------
----            Fonctions d'actualisation du catalogue à l'ajout d'un filtre            ----
--------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------*/


//Sélection du filtre Type -> Pull
function displayPulls()
{
    tableauCourant = getProdsbyType("Pull", tableauCourant.slice()); // Actualise la variable du tableau courrant
    fillCatalogue(tableauCourant); // Met à jour le catalogue affiché
}

//Sélection du filtre Type -> Tee
function displayTees()
{
    tableauCourant = getProdsbyType("Tee", tableauCourant.slice()); // Actualise la variable du tableau courrant
    fillCatalogue(tableauCourant); // Met à jour le catalogue affiché
}

//Sélection du filtre Couleur -> Une couleur
function displayColor(couleur)
{
    tableauCourant = getProdsbyColor(couleur, tableauCourant.slice()); // Actualise la variable du tableau courrant
    fillCatalogue(tableauCourant); // Met à jour le catalogue affiché
}

//Sélection du filtre Prix maximum -> Un prix
function displayPrix(formPrix)
{
    tableauCourant = getProdsbyPrix(formPrix.prix.value, tableauCourant.slice()); // Actualise la variable du tableau courrant
    fillCatalogue(tableauCourant); // Met à jour le catalogue affiché
}
// TESTS
// console.log(tableauCourant);
// displayPulls();
// console.log(tableauCourant);
// displayAll();
// console.log(tableauCourant);
// displayColor("Vert");
// console.log(tableauCourant);

