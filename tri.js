function triPrixCroissant()
{
    tableauCourant.sort((a, b) => (a.prixProd > b.prixProd) ? 1 : -1)
    fillCatalogue(tableauCourant);
}

function triPrixDecroissant()
{
    tableauCourant.sort((a, b) => (a.prixProd < b.prixProd) ? 1 : -1)
    fillCatalogue(tableauCourant);
}