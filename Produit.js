class Produit {
    constructor(type, numProd, libProd, prixProd, tailleProd, couleurProd, images) {
      this.type = type;
      this.numProd = numProd;
      this.libProd = libProd;
      this.prixProd = prixProd;
      this.tailleProd = tailleProd;
      this.couleurProd = couleurProd;
      this.images = images;
    }

    getType(){
        return this.type;
    }

    getNumProd(){
        return this.numProd;
    }

    getLibProd(){
        return this.libProd;
    }

    getPrixProd(){
        return this.prixProd;
    }

    getTailleProd(){
        return this.tailleProd;
    }

    getCouleurProd(){
        return this.CouleurProd;
    }

    getImages(){
        return this.images;
    }

  }